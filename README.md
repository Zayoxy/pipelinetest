# Zayoxy/PipelineTest

Getting started with Gitlab CI/CD pipelines

## Tuto Video

I was watching this [video](https://www.youtube.com/watch?v=PGyhBwLyK2U) as I trained with pipelines

## What I learned

- The pipelines are written in YAML
- They run every time we do a commit in a Gitlab runner
- The Gitlab runner creates a docker, run the pipeline and delete it as soon as it is finished
- We create jobs that are executed in the order that we define in the stages
